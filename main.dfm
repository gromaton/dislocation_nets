object Form1: TForm1
  Left = 261
  Top = 6
  Width = 687
  Height = 640
  AutoSize = True
  BorderWidth = 5
  Caption = 'Dislocation nets'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnMouseMove = FormMouseMove
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Gauge1: TGauge
    Left = 0
    Top = 548
    Width = 141
    Height = 22
    ForeColor = clBlue
    Progress = 0
  end
  object Label16: TLabel
    Left = 37
    Top = 48
    Width = 13
    Height = 27
    Caption = 's'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Symbol'
    Font.Pitch = fpVariable
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 48
    Top = 65
    Width = 10
    Height = 13
    Caption = 'xx'
  end
  object OffsetLabel: TLabel
    Left = 576
    Top = 167
    Width = 42
    Height = 13
    Caption = 'Offset, %'
  end
  object StringGrid1: TStringGrid
    Left = 192
    Top = 5
    Width = 469
    Height = 150
    ColCount = 7
    DefaultRowHeight = 20
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    PopupMenu = PopupMenu1
    ScrollBars = ssVertical
    TabOrder = 0
    OnMouseDown = StringGrid1MouseDown
    OnSelectCell = StringGrid1SelectCell
    ColWidths = (
      64
      73
      96
      72
      76
      59
      21)
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 185
    Height = 185
    Caption = 'Heterostructure'
    TabOrder = 1
    object Label9: TLabel
      Left = 8
      Top = 114
      Width = 79
      Height = 13
      Caption = 'Film thickness, A'
    end
    object LabelX: TLabel
      Left = 58
      Top = 81
      Width = 11
      Height = 13
      Caption = 'x='
      Enabled = False
    end
    object StaticText1: TStaticText
      Left = 8
      Top = 139
      Width = 101
      Height = 17
      Caption = 'Substrate orientation'
      TabOrder = 0
    end
    object ComboBoxInterface: TComboBox
      Left = 104
      Top = 155
      Width = 57
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'Preset'
      OnChange = ComboBoxInterfaceChange
      Items.Strings = (
        '0 0 1'
        '1 1 1'
        '0 1 1')
    end
    object SpinEditR: TSpinEdit
      Left = 72
      Top = 155
      Width = 33
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 1
      OnChange = SpinEditRChange
    end
    object SpinEditQ: TSpinEdit
      Left = 40
      Top = 155
      Width = 33
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 0
      OnChange = SpinEditQChange
    end
    object SpinEditP: TSpinEdit
      Left = 8
      Top = 155
      Width = 33
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 4
      Value = 0
      OnChange = SpinEditPChange
    end
    object StaticText2: TStaticText
      Left = 8
      Top = 18
      Width = 49
      Height = 17
      Caption = 'Substrate'
      TabOrder = 5
    end
    object StaticText3: TStaticText
      Left = 8
      Top = 48
      Width = 22
      Height = 17
      Caption = 'Film'
      TabOrder = 6
    end
    object EditFilmThickness: TEdit
      Left = 112
      Top = 110
      Width = 65
      Height = 21
      TabOrder = 7
      Text = '200'
    end
    object ComboBoxSub: TComboBox
      Left = 72
      Top = 16
      Width = 65
      Height = 21
      DropDownCount = 20
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 8
      Text = 'Si'
      OnChange = ComboBoxSubChange
      Items.Strings = (
        'Si'
        'Ge')
    end
    object ComboBoxFilm2: TComboBox
      Left = 111
      Top = 69
      Width = 50
      Height = 21
      Enabled = False
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 9
      Text = 'Si'
      OnChange = ComboBoxFilm2Change
      Items.Strings = (
        'Si')
    end
    object CheckBoxSolSol: TCheckBox
      Left = 40
      Top = 48
      Width = 113
      Height = 17
      Caption = 'Solid solution'
      TabOrder = 10
      OnClick = CheckBoxSolSolClick
    end
    object EditX: TEdit
      Left = 72
      Top = 78
      Width = 30
      Height = 21
      Enabled = False
      TabOrder = 11
      Text = '0.12'
      OnExit = EditXExit
    end
    object ComboBoxFilm: TComboBox
      Left = 8
      Top = 69
      Width = 50
      Height = 21
      ItemHeight = 13
      ItemIndex = 1
      TabOrder = 12
      Text = 'Ge'
      OnChange = ComboBoxFilmChange
      Items.Strings = (
        'Si'
        'Ge')
    end
  end
  object ButtonAddFamily: TButton
    Left = 192
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = ButtonAddFamilyClick
  end
  object CheckBoxDF1: TCheckBox
    Left = 644
    Top = 30
    Width = 17
    Height = 17
    Color = clBtnHighlight
    Enabled = False
    ParentColor = False
    TabOrder = 3
    OnClick = CheckBoxDF1Click
  end
  object ButtonDeleteFamily: TButton
    Left = 264
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 4
    OnClick = ButtonDeleteFamilyClick
  end
  object GroupBox2: TGroupBox
    Left = 408
    Top = 200
    Width = 225
    Height = 153
    Caption = 'Thickness distribution, F(z)'
    TabOrder = 5
    object Label4: TLabel
      Left = 8
      Top = 44
      Width = 110
      Height = 13
      Caption = 'Where on  surface (x,y)'
    end
    object Label5: TLabel
      Left = 168
      Top = 47
      Width = 3
      Height = 13
      Caption = ','
    end
    object Label6: TLabel
      Left = 8
      Top = 97
      Width = 80
      Height = 13
      Caption = 'Number of points'
    end
    object Label7: TLabel
      Left = 8
      Top = 70
      Width = 88
      Height = 13
      Caption = 'Z range, A:     from'
    end
    object Label8: TLabel
      Left = 136
      Top = 70
      Width = 9
      Height = 13
      Caption = 'to'
    end
    object Label18: TLabel
      Left = 5
      Top = 11
      Width = 13
      Height = 27
      Caption = 's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Symbol'
      Font.Pitch = fpVariable
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel
      Left = 16
      Top = 25
      Width = 10
      Height = 13
      Caption = 'xx'
    end
    object Label20: TLabel
      Left = 72
      Top = 16
      Width = 13
      Height = 20
      Caption = 'E'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EditPntX: TEdit
      Left = 127
      Top = 41
      Width = 40
      Height = 21
      TabOrder = 0
      Text = '0'
      OnExit = EditPntXExit
    end
    object EditPntY: TEdit
      Left = 173
      Top = 41
      Width = 40
      Height = 21
      TabOrder = 1
      Text = '0'
      OnExit = EditPntYExit
    end
    object Button2: TButton
      Left = 6
      Top = 124
      Width = 75
      Height = 21
      Caption = 'Calculate'
      TabOrder = 2
      OnClick = Button2Click
    end
    object EditPntsNum: TEdit
      Left = 104
      Top = 94
      Width = 41
      Height = 21
      TabOrder = 3
      Text = '201'
    end
    object EditZ1: TEdit
      Left = 100
      Top = 67
      Width = 33
      Height = 21
      TabOrder = 4
      Text = '10'
    end
    object EditZ2: TEdit
      Left = 152
      Top = 67
      Width = 33
      Height = 21
      TabOrder = 5
      Text = '200'
    end
    object RadioButtonSigma: TRadioButton
      Left = 29
      Top = 18
      Width = 17
      Height = 17
      Checked = True
      TabOrder = 6
      TabStop = True
    end
    object RadioButtonEnergy: TRadioButton
      Left = 88
      Top = 18
      Width = 17
      Height = 17
      TabOrder = 7
    end
    object RadioButtonU: TRadioButton
      Left = 160
      Top = 18
      Width = 33
      Height = 17
      Caption = 'U'
      TabOrder = 8
    end
  end
  object StatusBar1: TStatusBar
    Left = 144
    Top = 546
    Width = 513
    Height = 25
    Align = alNone
    Panels = <
      item
        Text = 'Status: Idle'
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object GroupBox3: TGroupBox
    Left = 240
    Top = 200
    Width = 161
    Height = 97
    Caption = 'Tensor component'
    TabOrder = 7
    object SpeedButton11: TSpeedButton
      Left = 42
      Top = 17
      Width = 23
      Height = 22
      GroupIndex = 1
      Down = True
      Caption = ' xx'
      Margin = 1
      OnClick = SpeedButton11Click
    end
    object SpeedButton12: TSpeedButton
      Left = 66
      Top = 17
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'xy'
      OnClick = SpeedButton11Click
    end
    object SpeedButton13: TSpeedButton
      Left = 90
      Top = 17
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'xz'
      OnClick = SpeedButton11Click
    end
    object SpeedButton21: TSpeedButton
      Left = 42
      Top = 41
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = ' yx'
      Margin = 1
      OnClick = SpeedButton11Click
    end
    object SpeedButton22: TSpeedButton
      Left = 66
      Top = 41
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'yy'
      OnClick = SpeedButton11Click
    end
    object SpeedButton23: TSpeedButton
      Left = 90
      Top = 41
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'yz'
      OnClick = SpeedButton11Click
    end
    object SpeedButton31: TSpeedButton
      Left = 42
      Top = 65
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'zx'
      OnClick = SpeedButton11Click
    end
    object SpeedButton32: TSpeedButton
      Left = 66
      Top = 65
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'zy'
      OnClick = SpeedButton11Click
    end
    object SpeedButton33: TSpeedButton
      Left = 90
      Top = 65
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'zz'
      OnClick = SpeedButton11Click
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 360
    Width = 513
    Height = 177
    Caption = 'Film Elastic Energy Calculation'
    TabOrder = 8
    object Label10: TLabel
      Left = 4
      Top = 21
      Width = 148
      Height = 13
      Caption = 'First iteration layer thickness, A:'
    end
    object Label11: TLabel
      Left = 29
      Top = 76
      Width = 141
      Height = 13
      Caption = 'Iteration count along X and Y:'
    end
    object Label12: TLabel
      Left = 80
      Top = 44
      Width = 75
      Height = 13
      Caption = 'Initial thickness:'
    end
    object Label21: TLabel
      Left = 14
      Top = 100
      Width = 126
      Height = 13
      Caption = 'Iteration count in first layer:'
    end
    object Label22: TLabel
      Left = 9
      Top = 124
      Width = 154
      Height = 13
      Caption = 'Iteration count in rest (2nd) layer:'
    end
    object Label3: TLabel
      Left = 224
      Top = 151
      Width = 6
      Height = 13
      Caption = '+'
    end
    object Label15: TLabel
      Left = 368
      Top = 151
      Width = 6
      Height = 13
      Caption = '='
    end
    object Label23: TLabel
      Left = 264
      Top = 12
      Width = 77
      Height = 13
      Caption = 'Integration path:'
    end
    object Edit2: TEdit
      Left = 176
      Top = 17
      Width = 41
      Height = 21
      TabOrder = 0
      Text = '25'
      OnChange = Edit2Change
    end
    object Edit5: TEdit
      Left = 176
      Top = 96
      Width = 41
      Height = 21
      TabOrder = 1
      Text = '100'
    end
    object Edit6: TEdit
      Left = 176
      Top = 120
      Width = 41
      Height = 21
      TabOrder = 2
      Text = '100'
    end
    object Edit7: TEdit
      Left = 176
      Top = 72
      Width = 41
      Height = 21
      TabOrder = 3
      Text = '50'
    end
    object Edit4: TEdit
      Left = 176
      Top = 40
      Width = 41
      Height = 21
      TabOrder = 4
      Text = '0'
    end
    object Button5: TButton
      Left = 8
      Top = 144
      Width = 75
      Height = 24
      Caption = 'Calculate'
      TabOrder = 5
      OnClick = Button5Click
    end
    object EditEnergy: TEdit
      Left = 96
      Top = 147
      Width = 121
      Height = 21
      TabOrder = 6
      Text = 'Deform energy'
    end
    object EditNucleus: TEdit
      Left = 240
      Top = 147
      Width = 121
      Height = 21
      TabOrder = 7
      Text = 'Dislocation core energy'
    end
    object EditSummary: TEdit
      Left = 384
      Top = 147
      Width = 121
      Height = 21
      TabOrder = 8
      Text = 'Total elastic energy'
    end
    object RadioButtonSquare: TRadioButton
      Left = 344
      Top = 10
      Width = 65
      Height = 17
      Caption = 'Square'
      Checked = True
      TabOrder = 9
      TabStop = True
    end
    object RadioButtonTriangle: TRadioButton
      Left = 416
      Top = 10
      Width = 89
      Height = 17
      Caption = 'Triangle'
      TabOrder = 10
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 200
    Width = 233
    Height = 145
    Caption = 'Stress field calc params'
    TabOrder = 9
    object Label13: TLabel
      Left = 5
      Top = 12
      Width = 13
      Height = 27
      Caption = 's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Symbol'
      Font.Pitch = fpVariable
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 16
      Top = 29
      Width = 10
      Height = 13
      Caption = 'xx'
    end
    object Label1: TLabel
      Left = 90
      Top = 24
      Width = 80
      Height = 13
      Caption = 'On the height, A:'
    end
    object Label2: TLabel
      Left = 9
      Top = 48
      Width = 131
      Height = 13
      Caption = 'Iteration count along Z axis:'
    end
    object Button3: TButton
      Left = 8
      Top = 118
      Width = 57
      Height = 21
      Caption = 'Fit Scale'
      TabOrder = 0
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 64
      Top = 118
      Width = 25
      Height = 21
      Caption = 'x2'
      TabOrder = 1
      OnClick = Button4Click
    end
    object Button1: TButton
      Left = 93
      Top = 118
      Width = 75
      Height = 21
      Caption = 'Calculate'
      TabOrder = 2
      OnClick = Button1Click
    end
    object EditHeight: TEdit
      Left = 174
      Top = 20
      Width = 43
      Height = 21
      TabOrder = 3
      Text = '100'
    end
    object EditPntCount: TEdit
      Left = 174
      Top = 44
      Width = 43
      Height = 21
      TabOrder = 4
      Text = '201'
    end
    object EditX0: TEdit
      Left = 8
      Top = 69
      Width = 41
      Height = 21
      TabOrder = 5
      Text = '-143.5'
    end
    object StaticText4: TStaticText
      Left = 52
      Top = 69
      Width = 129
      Height = 21
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'X range, A'
      TabOrder = 6
    end
    object EditX1: TEdit
      Left = 183
      Top = 69
      Width = 41
      Height = 21
      TabOrder = 7
      Text = '143.5'
    end
    object StaticText5: TStaticText
      Left = 52
      Top = 92
      Width = 129
      Height = 21
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Y range, A'
      TabOrder = 8
    end
    object EditY0: TEdit
      Left = 8
      Top = 92
      Width = 41
      Height = 21
      TabOrder = 9
      Text = '-143.5'
    end
    object EditY1: TEdit
      Left = 183
      Top = 92
      Width = 41
      Height = 21
      TabOrder = 10
      Text = '143.5'
    end
    object Button8: TButton
      Left = 176
      Top = 120
      Width = 41
      Height = 17
      Caption = 'Deform'
      TabOrder = 11
      OnClick = Button8Click
    end
  end
  object CheckBoxRomanov: TCheckBox
    Left = 240
    Top = 304
    Width = 137
    Height = 17
    Caption = 'calc based on Romanov'
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
  object CheckBoxMisfitStress: TCheckBox
    Left = 240
    Top = 328
    Width = 161
    Height = 17
    Caption = 'taking mismatch stress'
    TabOrder = 11
  end
  object EditOffset: TEdit
    Left = 624
    Top = 163
    Width = 33
    Height = 21
    TabOrder = 12
    Text = '50'
  end
  object MainMenu1: TMainMenu
    Left = 576
    Top = 65528
    object File1: TMenuItem
      Caption = '&File'
      object New1: TMenuItem
        Caption = '&New'
      end
      object Open1: TMenuItem
        Caption = '&Open...'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = '&Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save &As...'
        OnClick = SaveAs1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Print1: TMenuItem
        Caption = '&Print...'
      end
      object PrintSetup1: TMenuItem
        Caption = 'P&rint Setup...'
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'ini'
    Filter = 'Config files (*.ini)|*.ini'
    Left = 544
    Top = 65528
  end
  object OpenDialog1: TOpenDialog
    Filter = 'config files(*.ini)|*.ini'
    Left = 512
    Top = 65528
  end
  object PopupMenu1: TPopupMenu
    Left = 608
    Top = 65528
    object N3: TMenuItem
      Caption = 'Set min possible distance between MD'
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = 'Set min distance for all families'
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = 'Duplicate'
      OnClick = N5Click
    end
    object N6: TMenuItem
      Caption = 'Set offset'
      OnClick = N6Click
    end
  end
end
