unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, StdCtrls, Spin, IniFiles, mxcommon, mxvectors, Grids,
  CheckLst, DBGrids, dislmath, mxarrays, Buttons, Gauges, ComCtrls;

const
  DEFAULT_CONFIG_FILENAME = './default.ini';
  DATA_FILENAME = './materials.dat';
  DATA_ERROR_MESSAGE = 'Error in '+DATA_FILENAME;
  MAX_DISLOCATION_FAMILIES = 6;
  tmpMju = 5.64e+10;
  tmpNju = 0.272;
  //tmpMju = 6.72e+10;
  //tmpNju = 0.25;

type
  TWorkSheetConstants = record
    aSub: extended; //�������� ��������, ��������
    aFilm: extended; //�������� ������, ��������
    FilmThickness: extended; //������� ������, ��������
    Subsrate, Film: string; //��������� ����������� �������� � ������
    p,q,r: extended; //���������� ��
    DislocationFamiliesCount: integer; //���-�� �������������� ��������
  end;

  TFamily = record
    Direction: TVector;
    Burgers: TVector;
    BurgersProjection: TVector;
    Distance: extended;
    Height: extended; //������ �� ������� �������� ���������
    Offset: extended;
    Basis: TBasis;
  end;

  EOpenFile = class(Exception);
  EDataFileError = class(Exception);
  EConfigFileError = class(Exception);

  TNewThread = class(TThread)
  private
    Value: extended;
    procedure Calc;
  protected
    procedure Execute; override;
  end;

  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    PrintSetup1: TMenuItem;
    Print1: TMenuItem;
    N2: TMenuItem;
    SaveAs1: TMenuItem;
    Save1: TMenuItem;
    Open1: TMenuItem;
    New1: TMenuItem;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    StringGrid1: TStringGrid;
    GroupBox1: TGroupBox;
    StaticText1: TStaticText;
    ComboBoxInterface: TComboBox;
    SpinEditR: TSpinEdit;
    SpinEditQ: TSpinEdit;
    SpinEditP: TSpinEdit;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    ButtonAddFamily: TButton;
    CheckBoxDF1: TCheckBox;
    ButtonDeleteFamily: TButton;
    ComboBoxSub: TComboBox;
    ComboBoxFilm: TComboBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    EditPntX: TEdit;
    EditPntY: TEdit;
    Label5: TLabel;
    Button2: TButton;
    Label6: TLabel;
    EditPntsNum: TEdit;
    Label7: TLabel;
    EditZ1: TEdit;
    Label8: TLabel;
    EditZ2: TEdit;
    Label9: TLabel;
    EditFilmThickness: TEdit;
    PopupMenu1: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    StatusBar1: TStatusBar;
    Gauge1: TGauge;
    GroupBox3: TGroupBox;
    SpeedButton11: TSpeedButton;
    SpeedButton12: TSpeedButton;
    SpeedButton13: TSpeedButton;
    SpeedButton21: TSpeedButton;
    SpeedButton22: TSpeedButton;
    SpeedButton23: TSpeedButton;
    SpeedButton31: TSpeedButton;
    SpeedButton32: TSpeedButton;
    SpeedButton33: TSpeedButton;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    RadioButtonSigma: TRadioButton;
    RadioButtonEnergy: TRadioButton;
    Label20: TLabel;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Edit2: TEdit;
    Edit5: TEdit;
    Label11: TLabel;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit4: TEdit;
    Label12: TLabel;
    Button5: TButton;
    Label21: TLabel;
    Label22: TLabel;
    EditEnergy: TEdit;
    GroupBox5: TGroupBox;
    Button3: TButton;
    Button4: TButton;
    Label13: TLabel;
    Label14: TLabel;
    Button1: TButton;
    Label1: TLabel;
    EditHeight: TEdit;
    Label2: TLabel;
    EditPntCount: TEdit;
    EditX0: TEdit;
    StaticText4: TStaticText;
    EditX1: TEdit;
    StaticText5: TStaticText;
    EditY0: TEdit;
    EditY1: TEdit;
    EditNucleus: TEdit;
    Label3: TLabel;
    Label15: TLabel;
    EditSummary: TEdit;
    ComboBoxFilm2: TComboBox;
    CheckBoxSolSol: TCheckBox;
    EditX: TEdit;
    LabelX: TLabel;
    N5: TMenuItem;
    Label23: TLabel;
    RadioButtonSquare: TRadioButton;
    RadioButtonTriangle: TRadioButton;
    Button8: TButton;
    CheckBoxRomanov: TCheckBox;
    CheckBoxMisfitStress: TCheckBox;
    RadioButtonU: TRadioButton;
    N6: TMenuItem;
    EditOffset: TEdit;
    OffsetLabel: TLabel;
    //����������
    procedure AddNewParamToIni(Section, Key, Value: string);//������� ��� ���������� ������ ��������� � ����� ������������
    procedure DelNewParamToIni(Section, Key: string);//������� ��� �������� ��������� �� ������ ������������
    //
    procedure Exit1Click(Sender: TObject);
    procedure SaveAs1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ReadConfigFile(FileName: string);
    procedure SetFilmParameter;
    //procedure ReadDataFile(FileName: string);
    procedure WriteConfigFile(FileName: string);
    procedure WriteDislNetInfo2ConfigFile(ConfigIniFile: TIniFile);
    procedure ReadDislNetFromConfigFile(FileName: string);
    procedure Save1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure ButtonAddFamilyClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonDeleteFamilyClick(Sender: TObject);
    procedure CheckBoxDF1Click(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ComboBoxSubChange(Sender: TObject);
    procedure FillComboBoxSubFilm(); //���������� ������ � ������������� � ����� ������ DATA_FILENAME
    function GetContourName: string;
    procedure SetContourChecked(ContourName: string);
    function ReadDataParam(Section: string; Key: string): string;
    function CheckDataFileIntegrity(): boolean;
    procedure ComboBoxFilmChange(Sender: TObject); //������� ��� �������� ����������� ����� ������ DATA_FILENAME, ���� ��������, �� ������������ ���������� EDataFileError
    procedure StressCalc;
    function GetFamilyData(FamilyNumber: integer): TFamily; //���������� ������ ���������
    procedure ReadFamilies;
    function GetStressMatrix(x,y,z: extended): Matrix;
    function IsCore(Families: array of TFamily; Point: TVector; MainBasis: TBasis): boolean; //������� ���������� ����� �� ����� ������ ��������������� �������� ������� b
    procedure Button1Click(Sender: TObject);
    procedure SpinEditPChange(Sender: TObject);
    procedure SpinEditQChange(Sender: TObject);
    procedure SpinEditRChange(Sender: TObject);
    procedure ComboBoxInterfaceChange(Sender: TObject);
    procedure EditPntXExit(Sender: TObject);
    procedure EditPntYExit(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure CheckBoxSolSolClick(Sender: TObject);
    procedure EditXExit(Sender: TObject);
    procedure ComboBoxFilm2Change(Sender: TObject);
    procedure StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N5Click(Sender: TObject);
    function IsImageDislocationsUsed: boolean;
    
    function DeformationZZ(Sigma_XX, Sigma_YY, Sigma_ZZ, Sigma_misfit, tmpMju, tmpNju: extended): extended;
    function GetSystemDisplacement(x, y, H: extended): extended;
    procedure Button8Click(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure N6Click(Sender: TObject);
  private
    { Private declarations }
    ConfigFile: TIniFile;
    DataFile: TIniFile;
    CheckBoxTemplate: array of boolean;
    Worksheet: TWorkSheetConstants;
    Families: array of TFamily;
    UseImageDisl: boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  CheckBoxDF: array of TCheckBox;
  CI_X,CI_Y: byte; //������� ��������� �������
  NewThread: TNewThread;

implementation

uses Math, Types;

{$R *.dfm}
procedure TForm1.AddNewParamToIni(Section, Key, Value: string);
var
  SRec: TSearchRec;
  FileCount: integer;
begin
  FindFirst('./*.ini',faAnyFile,SRec);
  ConfigFile:=TIniFile.Create('./'+SRec.Name);
  ConfigFile.WriteString(Section,Key,Value);
  ConfigFile.Free;
  FileCount:=1;
  while FindNext(SRec)=0 do begin
    ConfigFile:=TIniFile.Create('./'+SRec.Name);
    ConfigFile.WriteString(Section, Key,Value);
    ConfigFile.Free;
    Inc(FileCount,1);
  end;
  ShowMessage(IntToStr(FileCount)+' file(s) edited');
end;

procedure TForm1.DelNewParamToIni(Section, Key: string);
var
  SRec: TSearchRec;
  FileCount: integer;
begin
  FindFirst('./*.ini',faAnyFile,SRec);
  ConfigFile:=TIniFile.Create('./'+SRec.Name);
  ConfigFile.DeleteKey(Section,Key);
  ConfigFile.Free;
  FileCount:=1;
  while FindNext(SRec)=0 do begin
    ConfigFile:=TIniFile.Create('./'+SRec.Name);
    ConfigFile.DeleteKey(Section,Key);
    ConfigFile.Free;
    Inc(FileCount,1);
  end;
  ShowMessage(IntToStr(FileCount)+' file(s) edited');
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.SaveAs1Click(Sender: TObject);
begin
  SaveDialog1.Filter:='Config files (*.ini)|*.ini';
  if SaveDialog1.Execute then begin
    WriteConfigFile(SaveDialog1.FileName);
  end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  StringGrid1.Cells[0,0]:='Families';
  StringGrid1.Cells[1,0]:='Direction';
  StringGrid1.Cells[2,0]:='Burgers Vector';
  StringGrid1.Cells[3,0]:='D (Angstrom)';
  StringGrid1.Cells[4,0]:='Offset (�)';
  StringGrid1.Cells[5,0]:='Height (�)';
  StringGrid1.Cells[0,1]:='          1';
  
  FillComboBoxSubFilm;
  ReadConfigFile(DEFAULT_CONFIG_FILENAME);
  CI_X:=1; CI_Y:=1; //�� ��������� ���������� xx
end;

procedure TForm1.SpinEditPChange(Sender: TObject);
begin
  Worksheet.p:=mx_StrToExt(SpinEditP.Text);
  //ShowMessage('ok');
end;

procedure TForm1.SpinEditQChange(Sender: TObject);
begin
  Worksheet.q:=mx_StrToExt(SpinEditQ.Text);
end;

procedure TForm1.SpinEditRChange(Sender: TObject);
begin
  Worksheet.r:=mx_StrToExt(SpinEditR.Text);
end;

procedure TForm1.SetFilmParameter;
begin
  if CheckBoxSolSol.Checked then begin
        Worksheet.aFilm:=mx_StrToExt(ReadDataParam('LatticeParameter',ComboBoxFilm.Text))*mx_StrToExt(EditX.Text)+(1-mx_StrToExt(EditX.Text))*mx_StrToExt(ReadDataParam('LatticeParameter',ComboBoxFilm2.Text));
        Worksheet.Film:=ComboBoxFilm.Text+' '+ComboBoxFilm2.Text;
  end
  else begin
    Worksheet.aFilm:=mx_StrToExt(ReadDataParam('LatticeParameter',ComboBoxFilm.Text));
    Worksheet.Film:=ComboBoxFilm.Text;
  end;
end;

procedure TForm1.ReadConfigFile(FileName: string);
var
  FamiliesCount: string;
  p,q,r: string;
  //SolSol: string;
  SolSol: boolean;
  Sub, Film, Film2, x, FilmThickness: string;
  i: integer;
begin
  //�������� �� ������ ������ (������, ���� ���������� ���� ������������ �� ���������)
  if not FileExists(DEFAULT_CONFIG_FILENAME) then begin
    WriteConfigFile(DEFAULT_CONFIG_FILENAME); //����� ���� ����.�� ���������
    exit;
  end; //
  //���� � �������� ����� ������ �������������� ����
  if not FileExists(FileName) then begin
    raise EOpenFile.Create('file '+FileName+' does not exist');
  end;
  //
  ConfigFile:=TIniFile.Create(FileName);
  //������ ����� ������������
  Sub:=ConfigFile.ReadString('Heterosystem','Substrate','Err');
  if ConfigFile.ReadString('Heterosystem','SolidSolution','Err')='yes' then begin
    SolSol:=true;
  end
  else SolSol:=false;
  Film:=ConfigFile.ReadString('Heterosystem','Film','Err');
  x:=ConfigFile.ReadString('Heterosystem','x','Err');
  Film2:=ConfigFile.ReadString('Heterosystem','Film2','Err');
  FilmThickness:=ConfigFile.ReadString('Heterosystem','FilmThickness','Err');
  p:=ConfigFile.ReadString('Interface','p','Err');
  q:=ConfigFile.ReadString('Interface','q','Err');
  r:=ConfigFile.ReadString('Interface','r','Err');
  FamiliesCount:=ConfigFile.ReadString('DislocationNet','FamiliesCount','Err');
  SetContourChecked(ConfigFile.ReadString('EnergySettings','Contour','Err'));
  ConfigFile.Free;
  //
  //���� ���������������� ���� ��������� �� ��������������� ��� ��������� �� ���������
  Worksheet.Subsrate:='Si';
  Worksheet.Film:='Ge';
  Worksheet.FilmThickness:=200;
  Worksheet.p:=0;
  Worksheet.q:=0;
  Worksheet.r:=1;
  Worksheet.DislocationFamiliesCount:=1;
  //
  try
    //�������� �� �����������
    if (p='Err')or(q='Err')or(r='Err')or(p='')or(q='')or(r='')or(FamiliesCount='Err')or(FamiliesCount='')or(Sub='')or(Film='') then begin
      raise Exception.Create(FileName+' file is broken...');
    end;
    //������ � ��������� �������
    try
      Worksheet.Subsrate:=Sub;
      Worksheet.aSub:=mx_StrToExt(ReadDataParam('LatticeParameter',Worksheet.Subsrate));
      //����������� ��������� �������
      if SolSol then begin
        Worksheet.aFilm:=mx_StrToExt(ReadDataParam('LatticeParameter',Film))*mx_StrToExt(x)+(1-mx_StrToExt(x))*mx_StrToExt(ReadDataParam('LatticeParameter',Film2));
        Worksheet.Film:=Film+' '+Film2;
      end
      else begin
        Worksheet.Film:=Film;
        Worksheet.aFilm:=mx_StrToExt(ReadDataParam('LatticeParameter',Film));
      end;
      //
      Worksheet.FilmThickness:=mx_StrToExt(FilmThickness);
      Worksheet.p:=mx_StrToExt(p);
      Worksheet.q:=mx_StrToExt(q);
      Worksheet.r:=mx_StrToExt(r);
      Worksheet.DislocationFamiliesCount:=StrToInt(FamiliesCount);
    except
      on EConvertError do raise Exception.Create(FileName+' file is broken...');
    end;
    //
  finally
    //��������� ����� �������������
    ComboBoxSub.ItemIndex:=ComboBoxSub.Items.IndexOf(Worksheet.Subsrate);
    ComboBoxFilm.ItemIndex:=ComboBoxFilm.Items.IndexOf(Film);
    ComboBoxFilm2.ItemIndex:=ComboBoxFilm.Items.IndexOf(Film2);
    EditX.Text:=x;
    CheckBoxSolSol.Checked:=SolSol;
    EditFilmThickness.Text:=FloatToStr(Worksheet.FilmThickness);
    SpinEditP.Text:=FloatToStr(Worksheet.p);
    SpinEditQ.Text:=FloatToStr(Worksheet.q);
    SpinEditR.Text:=FloatToStr(Worksheet.r);

    //��������� ����� �������������� �����
    if Worksheet.DislocationFamiliesCount>StringGrid1.RowCount-1 then begin
      with StringGrid1 do begin
        for i:=1 to Worksheet.DislocationFamiliesCount-StringGrid1.RowCount+1 do begin;
          ButtonAddFamily.Click;
        end;
      end;
    end;
    if Worksheet.DislocationFamiliesCount<StringGrid1.RowCount-1 then begin
      with StringGrid1 do begin
        for i:=StringGrid1.RowCount-1 downto Worksheet.DislocationFamiliesCount+1 do begin;
          CheckBoxDF[i-2].Checked:=true;
        end;
      end;
      ButtonDeleteFamily.Click;
    end;
    ReadDislNetFromConfigFile(FileName);
    //
  end;
end;

procedure TForm1.WriteConfigFile(FileName: string);
begin
  ConfigFile:=TIniFile.Create(FileName);
  try
    ConfigFile.WriteString('Heterosystem','Substrate',ComboBoxSub.Text);
    if CheckBoxSolSol.Checked then
      ConfigFile.WriteString('Heterosystem','SolidSolution','yes')
    else
      ConfigFile.WriteString('Heterosystem','SolidSolution','no');
    ConfigFile.WriteString('Heterosystem','Film',ComboBoxFilm.Text);
    ConfigFile.WriteString('Heterosystem','x',EditX.Text);
    ConfigFile.WriteString('Heterosystem','Film2',ComboBoxFilm2.Text);
    ConfigFile.WriteString('Heterosystem','FilmThickness',EditFilmThickness.Text);
    ConfigFile.WriteString('Interface','p',SpinEditP.Text);
    ConfigFile.WriteString('Interface','q',SpinEditQ.Text);
    ConfigFile.WriteString('Interface','r',SpinEditR.Text);
    ConfigFile.WriteString('DislocationNet','FamiliesCount',IntToStr(StringGrid1.RowCount-1));
    WriteDislNetInfo2ConfigFile(ConfigFile);
    ConfigFile.WriteString('EnergySettings','Contour',GetContourName);
    //ShowMessage(IntToStr(RadioButtonTriangle.ComponentIndex));
  finally
    ConfigFile.Free;
  end;
end;

procedure TForm1.Save1Click(Sender: TObject);
begin
  WriteConfigFile(DEFAULT_CONFIG_FILENAME);
end;

procedure TForm1.Open1Click(Sender: TObject);
begin
  try
  if OpenDialog1.Execute then begin
    ReadConfigFile(OpenDialog1.FileName);
  end;
  except
    on EOpenFile do Open1Click(Self);
  end;
end;

procedure TForm1.ButtonAddFamilyClick(Sender: TObject);
begin
  with StringGrid1 do begin
    if RowCount=2 then CheckBoxDF1.Enabled:=true;
    if RowCount<=MAX_DISLOCATION_FAMILIES then begin
      RowCount:=RowCount+1;
      Cells[0,RowCount-1]:='          '+IntToStr(RowCount-1);
      SetLength(CheckBoxDF,RowCount-2);
      CheckBoxDF[RowCount-3]:=TCheckBox.Create(Self);
      CheckBoxDF[RowCount-3].Parent:=Self;
      CheckBoxDF[RowCount-3].Left:=CheckBoxDF1.Left;
      CheckBoxDF[RowCount-3].Top:=CheckBoxDF1.Top+(DefaultRowHeight+1)*(RowCount-2);
      CheckBoxDF[RowCount-3].Width:=CheckBoxDF1.Width;
      CheckBoxDF[RowCount-3].Color:=CheckBoxDF1.Color;
      CheckBoxDF[RowCount-3].OnClick:=CheckBoxDF1Click;
    end;
    Worksheet.DislocationFamiliesCount:=RowCount-1;
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetLength(CheckBoxDF,0);
  SetLength(CheckBoxTemplate,0);
  SetLength(Families,0);
  WriteConfigFile(DEFAULT_CONFIG_FILENAME);
end;

procedure TForm1.ButtonDeleteFamilyClick(Sender: TObject);
var
  MaxCheckBoxCount, i,j, CheckBoxCount2Delete: integer;
  tmpRows: array of TStrings;
begin
  //������� ���������� ��������� �� ��������
  MaxCheckBoxCount:=Length(CheckBoxDF);
  if CheckBoxDF1.Checked then CheckBoxCount2Delete:=1 else CheckBoxCount2Delete:=0;
  for i:=0 to MaxCheckBoxCount-1 do begin
    if CheckBoxDF[i].Checked then Inc(CheckBoxCount2Delete);
  end;
  //

  if CheckBoxCount2Delete>=1 then begin //���� ����� ������� ������ ����
    j:=0;
    SetLength(tmpRows,StringGrid1.RowCount-CheckBoxCount2Delete-1);
    for i:=0 to Length(CheckBoxTemplate)-1 do begin
      if not CheckBoxTemplate[i] then begin
        tmpRows[j]:=StringGrid1.Rows[i+1];
        Inc(j);
      end;
    end;
    StringGrid1.RowCount:=StringGrid1.RowCount-CheckBoxCount2Delete;
    for i:=1 to StringGrid1.RowCount-1 do begin
      StringGrid1.Rows[i]:=tmpRows[i-1];
      StringGrid1.Cells[0,i]:='          '+IntToStr(i);
    end;
    for i:=Length(CheckBoxDF)-1 downto Length(CheckBoxDF)-CheckBoxCount2Delete do begin
      CheckBoxDF[i].Free;
    end;
    SetLength(CheckBoxDF,Length(CheckBoxDF)-CheckBoxCount2Delete);
    SetLength(tmpRows,0);
    with StringGrid1 do begin
      if RowCount=2 then CheckBoxDF1.Enabled:=false;
    end;
    CheckBoxDF1.Checked:=false;
    for i:=0 to Length(CheckBoxDF)-1 do begin
      CheckBoxDF[i].Checked:=false;
    end;
    Worksheet.DislocationFamiliesCount:=StringGrid1.RowCount-1;
  end;
end;

procedure TForm1.CheckBoxDF1Click(Sender: TObject);
var
  i, UnCheckedCount: integer;
  Template: array of boolean;
begin
  SetLength(CheckBoxTemplate,Length(CheckBoxDF)+1); //������������� ����� ������� ������ ������ ���-�� ���������
  if CheckBoxDF1.Checked then begin
    CheckBoxTemplate[0]:=true;
    UnCheckedCount:=0;
  end
  else begin
    CheckBoxTemplate[0]:=false;
    UnCheckedCount:=1;
  end;
  for i:=1 to Length(CheckBoxTemplate)-1 do begin
    if CheckBoxDF[i-1].Checked then begin
      CheckBoxTemplate[i]:=true;
    end
    else begin
      Inc(UnCheckedCount);
      CheckBoxTemplate[i]:=false;
    end;
  end;
  //ShowMessage(IntToStr(UnCheckedCount));
  if UnCheckedCount=1 then begin
    if not CheckBoxTemplate[0] then CheckBoxDF1.Enabled:=false
    else begin
      for i:=1 to Length(CheckBoxTemplate)-1 do begin
        if not CheckBoxTemplate[i] then begin
          CheckBoxDF[i-1].Enabled:=false;
        end;
      end;
    end;
  end
  else begin
    if not CheckBoxTemplate[0] then CheckBoxDF1.Enabled:=true;
    for i:=1 to Length(CheckBoxTemplate)-1 do begin
        if not CheckBoxTemplate[i] then begin
          CheckBoxDF[i-1].Enabled:=true;
        end;
      end;
  end;
end;

procedure TForm1.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ACol=6 then CanSelect:=false;
end;

procedure TForm1.ComboBoxSubChange(Sender: TObject);
begin
  Worksheet.Subsrate:=ComboBoxSub.Text;
  Worksheet.aSub:=mx_StrToExt(ReadDataParam('LatticeParameter',Worksheet.Subsrate));
end;

procedure TForm1.ComboBoxFilmChange(Sender: TObject);
begin
  //Worksheet.Film:=ComboBoxFilm.Text;
  //Worksheet.aFilm:=mx_StrToExt(ReadDataParam('LatticeParameter',Worksheet.Film));
  SetFilmParameter;
end;

procedure TForm1.FillComboBoxSubFilm;
begin
  //�������� �� �����������
  CheckDataFileIntegrity;
  //
  DataFile:=TIniFile.Create(DATA_FILENAME);
  try
    DataFile.ReadSection('LatticeParameter',ComboBoxSub.Items);
    DataFile.ReadSection('LatticeParameter',ComboBoxFilm.Items);
    DataFile.ReadSection('LatticeParameter',ComboBoxFilm2.Items);
    ComboBoxSub.ItemIndex:=0;
    ComboBoxFilm.ItemIndex:=1;
    ComboBoxSub.ItemIndex:=0;
  finally
    DataFile.Free;
  end;
end;

function TForm1.CheckDataFileIntegrity: boolean;
var
  LatticeParams, Puassons: TStrings;
  i: integer;
begin
  Result:=true;
  DataFile:=TIniFile.Create(DATA_FILENAME);
  LatticeParams:=TStringList.Create;
  Puassons:=TStringList.Create;
  try
    DataFile.ReadSection('LatticeParameter',LatticeParams);
    DataFile.ReadSection('Puasson',Puassons);
    for i:=0 to LatticeParams.Count-1 do begin
      //������� ��� ������� ���������, ��� ����������� ��������
      if (LatticeParams[i]<>Puassons[i])or(LatticeParams.Count<2) then begin
        raise EDataFileError.Create(DATA_ERROR_MESSAGE);
        break;
      end;
    end;
  finally
    DataFile.Free;
    LatticeParams.Free;
    Puassons.Free;
  end;
end;

function TForm1.ReadDataParam(Section, Key: string): string;
begin
  DataFile:=TIniFile.Create(DATA_FILENAME);
  try
    Result:=DataFile.ReadString(Section,Key,'Err');
    if Result='Err' then raise EDataFileError.Create(DATA_ERROR_MESSAGE);
  finally
    DataFile.Free;
  end;
end;

procedure TForm1.WriteDislNetInfo2ConfigFile(ConfigIniFile: TIniFile);
//===============================================================================================================
//  ��������� ������ � ��� �������� ���������������� ���� ���������� � �������������� ����������
//  ���������:
//        ConfigIniFile: TIniFile - ���� ������������ (������ ���� ������ �� ������ ���������� ������ ���������)
//===============================================================================================================
var
  i: integer;
begin
  //�������� ���������� ���������� � ����.�����
  for i:=1 to MAX_DISLOCATION_FAMILIES do begin
    ConfigIniFile.EraseSection('Net'+IntToStr(i));
  end;
  //
  for i:=1 to Worksheet.DislocationFamiliesCount do begin
    ConfigIniFile.WriteString('Net'+IntToStr(i),'FamilyDir',StringGrid1.Cells[1,i]);
    ConfigIniFile.WriteString('Net'+IntToStr(i),'FamilyBurgers',StringGrid1.Cells[2,i]);
    ConfigIniFile.WriteString('Net'+IntToStr(i),'FamilyDistance',StringGrid1.Cells[3,i]);
    ConfigIniFile.WriteString('Net'+IntToStr(i),'FamilyOffset',StringGrid1.Cells[4,i]);
    ConfigIniFile.WriteString('Net'+IntToStr(i),'FamilyHeight',StringGrid1.Cells[5,i]);
  end;
end;

procedure TForm1.ReadDislNetFromConfigFile(FileName: string);
//===============================================================================================================
//  ��������� ���������� ���������� � �������������� ���������� � ���������� ����� StringGrid1
//  ���������:
//        FileName: string - ��� ����� ������������
//===============================================================================================================
var
  i: integer;
  str: string;
begin
  ConfigFile:=TIniFile.Create(FileName);
  try
    for i:=1 to Worksheet.DislocationFamiliesCount do begin
      StringGrid1.Cells[1,i]:=ConfigFile.ReadString('Net'+IntToStr(i),'FamilyDir','Err');
      StringGrid1.Cells[2,i]:=ConfigFile.ReadString('Net'+IntToStr(i),'FamilyBurgers','Err');
      StringGrid1.Cells[3,i]:=ConfigFile.ReadString('Net'+IntToStr(i),'FamilyDistance','Err');
      StringGrid1.Cells[4,i]:=ConfigFile.ReadString('Net'+IntToStr(i),'FamilyOffset','Err');
      StringGrid1.Cells[5,i]:=ConfigFile.ReadString('Net'+IntToStr(i),'FamilyHeight','Err');
    end;
    //ShowMessage(ConfigFile.ReadString('Net'+IntToStr(2),'FamilyDistance','Err'));
  finally
    ConfigFile.Free;
  end;
end;

procedure TForm1.StressCalc;
begin
//��� � - ����������� �������������� ����� ������� ���������
//��� y - ��������������� ��� � � ����� � ��������� ������� ������
//��� z - ������������� � ��

end;

function TForm1.GetStressMatrix(x, y, z: extended): Matrix;
var
  SK: TBasis; //����� ������� ���������
  SK_new: TBasis; //������� ��������� ������� ���������
  TMat: Matrix; //������� ������������ ��������� �� SK � SK_new ����������� ��������� ��������� � ��� ���������� ��������������
  vPnt: TVector; //���������� ����� � �� ������� ���������
  StressTenzor: Matrix; //��� �������� ������� ������ �� �������� � ����� ��
  BurgersX,BurgersY,BurgersZ: extended; //�������� �������� �������� (�������� ����������, ������� � ��������� ��� �����������)
  //Family: TFamily; //������ ���������� �� ����� �� ��������
  i: integer;
  lmn: TVector; //������� � ������� ��� ������� ����� ��������� sigma ����������
  SigmaN: extended; //����� ���������� ����������� �� �������� � �������� lmn (��� �������� ���������� ��������������)
begin
  //��� � - ����������� �������������� ����� ������� ���������
  //����������� ������� ��
  if z=0 then begin
  end;
  SK.X:=StrToVec(StringGrid1.Cells[1,1]);
  SK.Z:=GetVector(Worksheet.p,Worksheet.q,Worksheet.r);
  //SK.X:=RotateVector(SK.Z,Families[1].Direction,DegToRad(45));
  SK.Y:=VecxVec(SK.Z,SK.X);
  //
  Result:=MATRIX_NULL;
  {if IsCore(Families,GetVector(x,y,z),SK) then begin   //�������� �� ��������� � ����, ���� �� �� �� �������
    exit;
  end; }
  //
  //������ �������� ���������� ��� ��������� ��������
  for i:=0 to Worksheet.DislocationFamiliesCount-1 do begin //�������� �� ������� ���������
    TMat:=GetTransitionMatrix(SK,Families[i+1].Basis);
    vPnt:=MatxVec(TMat,GetVector(x,y,z-Families[i+1].Height));
    vPnt.y:=vPnt.y-Families[i+1].Offset;
    //StressTenzor:=GetStressMatrixFamily(Families[i+1].BurgersProjection.x,Families[i+1].BurgersProjection.y,Families[i+1].BurgersProjection.z,Families[i+1].Distance,vPnt.y,vPnt.z,tmpMju,tmpNju);
    if CheckBoxRomanov.Checked then begin
      StressTenzor:=GetStressMatrixFamilyR(Families[i+1].BurgersProjection.x,Families[i+1].BurgersProjection.y,Families[i+1].BurgersProjection.z,Families[i+1].Distance,vPnt.y,vPnt.z, StrToFloat(EditFilmThickness.Text),tmpMju,tmpNju);
    end
    else begin
      StressTenzor:=GetStressMatrixFamily(Families[i+1].BurgersProjection.x,Families[i+1].BurgersProjection.y,Families[i+1].BurgersProjection.z,Families[i+1].Distance,vPnt.y,vPnt.z,tmpMju,tmpNju);
    end;

    StressTenzor:=MatxMat(MatxMat(InvertMatrix(TMat),StressTenzor),TMat);
    //SigmaN:=StressTenzor[1,1]*sqr(lmn.x)+StressTenzor[2,2]*sqr(lmn.y)+StressTenzor[3,3]*sqr(lmn.z)+2*StressTenzor[2,3]*lmn.y*lmn.z+2*StressTenzor[1,3]*lmn.x*lmn.z+2*StressTenzor[1,2]*lmn.x*lmn.y;
    Result:=SumMats(Result,StressTenzor);
  end;
  //ShowMessage(FloatToStr(SigmaN));
end;

function TForm1.GetSystemDisplacement(x, y, H: extended): extended;
var
  i: integer;
  TMat: Matrix; //������� ������������ ��������� �� SK � SK_new ����������� ��������� ��������� � ��� ���������� ��������������
  SK: TBasis; //����� ������� ���������
  vPnt: TVector; //���������� ����� � �� ������� ���������
begin
  Result:=0;
  ReadFamilies;
  SK.X:=StrToVec(StringGrid1.Cells[1,1]);
  SK.Z:=GetVector(Worksheet.p,Worksheet.q,Worksheet.r);
  //SK.X:=RotateVector(SK.Z,Families[1].Direction,DegToRad(45));
  SK.Y:=VecxVec(SK.Z,SK.X);
  for i:=0 to Worksheet.DislocationFamiliesCount-1 do begin //�������� �� ������� ���������
    TMat:=GetTransitionMatrix(SK,Families[i+1].Basis);
    vPnt:=MatxVec(TMat,GetVector(x,y,0));
    vPnt.y:=vPnt.y-Families[i+1].Offset;
    //StressTenzor:=GetStressMatrixFamily(Families[i+1].BurgersProjection.x,Families[i+1].BurgersProjection.y,Families[i+1].BurgersProjection.z,Families[i+1].Distance,vPnt.y,vPnt.z,tmpMju,tmpNju);
    Result:=Result+DisplacementZ(Families[i+1].BurgersProjection.y, H ,Families[i+1].Distance,vPnt.y);
  end;
//
end;

function TForm1.IsCore(Families: array of TFamily; Point: TVector; MainBasis: TBasis): boolean;
var
  Ys: extended;
  i, n: integer;
  A: TVector;
begin
//�� ��������� �� ������ � ����
  Result:=false;
//�������� ��������� � ���� ������
  for i:=1 to Worksheet.DislocationFamiliesCount do begin
    Ys:=Point.x*cos(AngleBtwnVecs(MainBasis.X,Families[i].Basis.Y))+Point.y*cos(AngleBtwnVecs(MainBasis.Y,Families[i].Basis.Y));
    //Ys:=Point.x*cos(AngleBtwnVecs(MainBasis.Y,Families[i].Basis.X))+Point.y*cos(AngleBtwnVecs(MainBasis.Y,Families[i].Basis.Y));
    n:=round((Ys-Families[i].Offset)/Families[i].Distance);
    if (sqr(Ys-Families[i].Offset-Families[i].Distance*n)+sqr(Point.z))<=(sqr(VecLen(Families[i].Burgers))) then begin
      Result:=true;
      break;
    end;
  end;
end;


procedure TForm1.Button1Click(Sender: TObject);
var
  DataFile: TextFile;
  x0,x1,y0,y1,x,y,z,hx,hy: extended;
  n: integer; //���������� ����� ����� ���
  i,j: integer;
  S: Matrix; //������ ����������
  Sigma_misfit: extended; //���������� ��������������
  f,b: extended; //�������� ��������������, ������ ��������
  D_max: extended;
  Counter,IterCount: integer; //�������, ��� ��������
  Mult: extended;
  EpsZZ: extended;
begin
  SaveDialog1.Filter:='Data Files (*.dat)|*.dat';
  if SaveDialog1.Execute then begin
    StatusBar1.Panels.Items[0].Text:='Status: Processing..';
    Counter:=0; //��������� ������� � ������� ���������
    f:=Worksheet.aFilm/Worksheet.aSub-1;
    ReadFamilies;
    b:=ScalarMult(Families[1].Burgers,NormVec(VecxVec(GetVector(Worksheet.p,Worksheet.q,Worksheet.r),Families[1].Direction)) ); //����������� ������� ����������
    //D_max:=b/f;
    //D_max:=143.5;
    {for i:=1 to Worksheet.DislocationFamiliesCount do begin
      StringGrid1.Cells[3,i]:=FloatToStr(D_max);
    end;   }
    if IsImageDislocationsUsed then Mult:=2 else Mult:=1;
    Sigma_misfit:=SigmaMisfit(Worksheet.aSub,Worksheet.aFilm,tmpMju,tmpNju,Families[1].Distance, Mult);
    //ShowMessage(FloatToStr(D_max)+' '+FloatToStr(Sigma_misfit));
    //ReadFamilies;
    z:=mx_StrToExt(EditHeight.Text);
    n:=StrToInt(EditPntCount.Text);
    IterCount:=Round(n*n/100);
    x0:=mx_StrToExt(EditX0.Text);
    x1:=mx_StrToExt(EditX1.Text);
    y0:=mx_StrToExt(EditY0.Text);
    y1:=mx_StrToExt(EditY1.Text);
    hx:=(x1-x0)/(n-1);
    hy:=(y1-y0)/(n-1);
    Rewrite(DataFile,SaveDialog1.FileName);
    x:=x0; y:=y0;
    for j:=1 to n do begin
      for i:=1 to n do begin
        S:=GetStressMatrix(x,y,z);
        EpsZZ:=DeformationZZ(S[1,1],S[2,2],S[3,3], Sigma_misfit, tmpMju, tmpNju);
        Writeln(DataFile,x,'; ',y,'; ',S[CI_X,CI_Y]{+Sigma_misfit});
        //Writeln(DataFile,x,'; ',y,'; ',EpsZZ);
        Inc(Counter);
        if Counter mod IterCount = 0 then begin
          Gauge1.Progress:=Gauge1.Progress+1;
          Application.ProcessMessages;
        end;
        x:=x+hx;
      end;
      x:=x0; y:=y+hy;
    end;
    Gauge1.Progress:=0;
    CloseFile(DataFile);
    StatusBar1.Panels.Items[0].Text:='Status: Done';
  end;
end;

function TForm1.GetFamilyData(FamilyNumber: integer): TFamily;
var
  StrArr: TStrArray;
  strDev: string; //���� ��������� �������� ��� ����� ������� �������� (��������, ���� ������ = a/2 1 1 0, �� �������� ����� 2
begin
  //
  SetLength(StrArr,4);
  try
    StrArr:=mx_arr_explode(' ',StringGrid1.Cells[2,FamilyNumber]);
    strDev:=Copy(StrArr[0],3,Length(StrArr[0]));
    Result.Burgers:=GetVector(mx_StrToExt(StrArr[1]),mx_StrToExt(StrArr[2]),mx_StrToExt(StrArr[3]));
    Result.Burgers:=SetVecLen(Result.Burgers,Worksheet.aFilm/mx_StrToExt(strDev)*VecLen(Result.Burgers));
  finally
    SetLength(StrArr,0);
  end;

  Result.Direction:=StrToVec(StringGrid1.Cells[1,FamilyNumber]);
  Result.Distance:=mx_StrToExt(StringGrid1.Cells[3,FamilyNumber]);
  Result.Offset:=mx_StrToExt(StringGrid1.Cells[4,FamilyNumber]);
  Result.Height:=mx_StrToExt(StringGrid1.Cells[5,FamilyNumber]);
  Result.Basis.X:=NormVec(Result.Direction);
  Result.Basis.Z:=NormVec(GetVector(Worksheet.p,Worksheet.q,Worksheet.r));
  Result.Basis.Y:=NormVec(VecxVec(Result.Basis.Z,Result.Basis.X));
  Result.BurgersProjection.x:=ScalarMult(Result.Burgers,Result.Basis.X); //����������� �������� ����������
  Result.BurgersProjection.y:=ScalarMult(Result.Burgers,Result.Basis.Y); //����������� ������� ����������
  Result.BurgersProjection.z:=ScalarMult(Result.Burgers,Result.Basis.Z); //����������� ����������� ����������
end;

procedure TForm1.ComboBoxInterfaceChange(Sender: TObject);
var
  PQR: TVector;
begin
  PQR:=StrToVec(ComboBoxInterface.Text,' ');
  SpinEditP.Text:=FloatToStr(PQR.x);
  SpinEditQ.Text:=FloatToStr(PQR.y);
  SpinEditR.Text:=FloatToStr(PQR.z);
end;

procedure TForm1.ReadFamilies;
var
  i: integer;
begin
  //
  SetLength(Families,Worksheet.DislocationFamiliesCount+1);
  for i:=1 to Worksheet.DislocationFamiliesCount do begin
    Families[i]:=GetFamilyData(i);
  end;
end;

procedure TForm1.EditPntXExit(Sender: TObject);
begin
  if (EditPntX.Text='')or(EditPntX.Text[1]=' ') then EditPntX.Text:='x';
end;

procedure TForm1.EditPntYExit(Sender: TObject);
begin
  if (EditPntY.Text='')or(EditPntY.Text[1]=' ') then EditPntY.Text:='y';
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  DataFile: TextFile;
  S: Matrix; //������ ����������
  Sigma_misfit, Sigma_misfit1: extended; //���������� ��������������
  z,z1,z2,dz: extended;
  i,n: integer;
  f,b: extended; //�������� ��������������, ������ ��������
  D_lim: extended; //���������� ����� ������������ ��� 100%
  Counter,IterCount: integer; //�������, ��� ��������
  E: extended; //�������
  Mult: extended;
  //SigmaSurf: Matrix;   //**************************************************************************//
begin
  SaveDialog1.Filter:='Data Files (*.dat)|*.dat';
  if SaveDialog1.Execute then begin
    StatusBar1.Panels.Items[0].Text:='Status: Processing..';
    //f:=Worksheet.aFilm/Worksheet.aSub-1;
    f:=(Worksheet.aFilm-Worksheet.aSub)/Worksheet.aFilm;
    ReadFamilies;
    b:=ScalarMult(Families[1].Burgers,NormVec(VecxVec(GetVector(Worksheet.p,Worksheet.q,Worksheet.r),Families[1].Direction)) ); //����������� ������� ����������
    //D_lim:=3*b/f/2;
    //D_lim:=143.5;
    {for i:=1 to Worksheet.DislocationFamiliesCount do begin
      StringGrid1.Cells[3,i]:=FloatToStr(D_lim);
    end;}
    if IsImageDislocationsUsed then Mult:=2 else Mult:=1;
    Sigma_misfit:=SigmaMisfit(Worksheet.aSub,Worksheet.aFilm,tmpMju,tmpNju,Families[1].Distance, 2);
 //   Sigma_misfit1:=2*tmpMju*f*(1+tmpNju)/(1-tmpNju);
 //   ShowMessage(FloatToStr(Sigma_misfit1+Sigma_misfit));
 //   exit;
    //ReadFamilies;
    z1:=mx_StrToExt(EditZ1.Text);
    z2:=mx_StrToExt(EditZ2.Text);
    n:=StrToInt(EditPntsNum.Text);
    dz:=(z2-z1)/(n-1);
    z:=z1;
    IterCount:=Round(n/100);
    if IterCount=0 then IterCount:=1;
    Rewrite(DataFile,SaveDialog1.FileName);
    //SigmaSurf:=GetStressMatrix(mx_StrToExt(EditPntX.Text),mx_StrToExt(EditPntY.Text),mx_StrToExt(EditFilmThickness.Text)); //*******************************************//
    try
      for i:=1 to n do begin
        if RadioButtonEnergy.Checked then begin
          S:=GetStressMatrix(mx_StrToExt(EditPntX.Text),mx_StrToExt(EditPntY.Text),z);
          E:=EnergyInPoint(S,Sigma_misfit,tmpMju,tmpNju,tmpMju*2*(1+tmpNju),1,1,1);
          Writeln(DataFile,z,'; ',E);
        end
        else if RadioButtonSigma.Checked then begin
          S:=GetStressMatrix(mx_StrToExt(EditPntX.Text),mx_StrToExt(EditPntY.Text),z);
          if (CheckBoxMisfitStress.Checked) and (z>=0) then begin
            Writeln(DataFile,z,'; ',S[CI_X,CI_Y]+Sigma_misfit);
          end
          else begin
            Writeln(DataFile,z,'; ',S[CI_X,CI_Y]);
          end;
        end
        else if RadioButtonU.Checked then begin
          E:=GetSystemDisplacement(mx_StrToExt(EditPntX.Text),mx_StrToExt(EditPntY.Text),z);
          Writeln(DataFile,z,'; ',E);
        end;
        z:=z+dz;
        Inc(Counter);
        if Counter mod IterCount = 0 then begin
          Gauge1.Progress:=Gauge1.Progress+1;
          Application.ProcessMessages;
        end;
      end;
    finally
      CloseFile(DataFile);
      Gauge1.Progress:=0;
    end;
    StatusBar1.Panels.Items[0].Text:='Status: Done';

  end;
end;


procedure TForm1.SpeedButton11Click(Sender: TObject);
begin
  CI_X:=StrToInt(Copy((Sender as TSpeedButton).Name,12,1));
  CI_Y:=StrToInt(Copy((Sender as TSpeedButton).Name,13,1));
  Label14.Caption:=Trim((Sender as TSpeedButton).Caption);
  Label19.Caption:=Trim((Sender as TSpeedButton).Caption);
  //ShowMessage(IntToStr(CI_X)+' '+IntToStr(CI_Y));
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  EditX0.Text:='-'+StringGrid1.Cells[3,1];
  EditX1.Text:=StringGrid1.Cells[3,1];
  EditY0.Text:='-'+StringGrid1.Cells[3,1];
  EditY1.Text:=StringGrid1.Cells[3,1];
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  EditX0.Text:=FloatToStr(mx_StrToExt(EditX0.Text)*2);
  EditX1.Text:=FloatToStr(mx_StrToExt(EditX1.Text)*2);
  EditY0.Text:=FloatToStr(mx_StrToExt(EditY0.Text)*2);
  EditY1.Text:=FloatToStr(mx_StrToExt(EditY1.Text)*2);
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  En, En_nuc: extended; //������� ������� ���������� � ������� ����
  LD: extended; //����� ���������� � �������
  E: extended; //������ ����
  SM: Matrix;
  x,y,z,dx,dy,dz: extended;
  i,j,k: integer;
  L,L2: extended; //������� ������������
  N, Nz1,Nz2: integer; //���������� ��������� �� ������� � �� ������ (� ������ ���� � ������)
  H1,H2: extended; //������� ������� � ������� ����
  //SFrame: extended; //������� �������
  Counter,IterCount: integer; //�������, ��� ��������
  Sigma_misfit,f: extended;
  EnergyFile: TextFile;
  NucleiFile: TextFile;
  MainBasis: TBasis;
  Mult: extended;
  En_tmp: extended;
begin

  {NewThread:=TNewThread.Create(true);
  NewThread.FreeOnTerminate:=true;
  NewThread.Resume;}
  EditSummary.Text:='';
  StatusBar1.Panels.Items[0].Text:='Status: Processing..';
  E:=tmpMju*2*(1+tmpNju);
  Counter:=0;
  WriteConfigFile(DEFAULT_CONFIG_FILENAME);
  ReadConfigFile(DEFAULT_CONFIG_FILENAME);
  ReadFamilies;
  MainBasis.X:=NormVec(Families[1].Direction);
  MainBasis.Z:=GetVector(Worksheet.p,Worksheet.q,Worksheet.r);
  MainBasis.Y:=VecxVec(MainBasis.Z,MainBasis.X);
  //f:=Worksheet.aFilm/Worksheet.aSub-1;
  f:=(Worksheet.aFilm-Worksheet.aSub)/Worksheet.aFilm;
  //f:=abs(2*(Worksheet.aSub-Worksheet.aFilm)/(Worksheet.aSub+Worksheet.aFilm));
  if IsImageDislocationsUsed then Mult:=2 else Mult:=1;
  Sigma_misfit:=SigmaMisfit(Worksheet.aSub,Worksheet.aFilm,tmpMju,tmpNju,2);
  En:=0;
  L:=Families[1].Distance;
  if RadioButtonSquare.Checked then begin
    if Worksheet.DislocationFamiliesCount>1 then
      L2:=Families[2].Distance
    else
      L2:=L;
    LD:=L+L2;  //���������� ����� ���������� � �������� ����������
  end
  else begin
    //L2:=Families[1].Distance/tan(60*Pi/180)*3;
    L2:=L/tan(60*Pi/180);
    LD:=L/sin(60*Pi/180)+L2;
  end;
  En_nuc:=EnergyNucleus(sqrt(sqr(Families[1].BurgersProjection.y)+sqr(Families[1].BurgersProjection.z)),Families[1].BurgersProjection.x,LD,tmpMju,tmpNju);
  EditNucleus.Text:=FloatToStr(En_nuc/((L)*(L2)*sqr(1e-10))); //����� ������� ��������������� ����
  //exit;
  z:=StrToFloat(Edit4.Text); //��������� ������
  N:=StrToInt(Edit7.Text);  //���-�� ��������� ����� X � Y
  H1:=StrToFloat(Edit2.Text);//Families[1].Distance/2;   //������� ������� ����
  H2:=Worksheet.FilmThickness-H1; //������� ������� ����
  Nz1:=StrToInt(Edit5.Text); //���-�� ��������� � ������ ����
  Nz2:=StrToInt(Edit6.Text); //���-�� ��������� �� ������ ����

  if CompareValue(Worksheet.FilmThickness,H1)=GreaterThanValue then  //���-�� �������� � 1% ��������
    IterCount:=Round(N*N*(Nz1+Nz2)/100)
  else
    IterCount:=Round(N*N*(Nz1)/100);

  dx:=L2/N;  dy:=L/N;

  //������ ���������� ���� � ����
  Rewrite(NucleiFile,'nuclei.txt');


  //������ �� ������� ����
  if z<H1 then begin
    dz:=(H1-z)/Nz1;
    for k:=1 to Nz1 do begin
      z:=z+dz;
      y:=-dy/2;
      for j:=1 to N do begin
        x:=-dx/2;
        y:=y+dy;
        for i:=1 to N do begin
          x:=x+dx;
          SM:=GetStressMatrix(x,y,z);
          if IsEqualMatrix(SM,MATRIX_NULL) and (z=StrToFloat(Edit4.Text)+dz)then begin
            Writeln(NucleiFile,FloatToStr(x)+'; '+FloatToStr(y));
          end;
          //En_tmp:=EnergyInPoint(SM,Sigma_misfit,tmpMju,tmpNju,E,1,1,1);
          //Writeln(NucleiFile,FloatToStr(x)+'; '+FloatToStr(y)+'; '+FloatToStr(EnergyInPoint(SM,Sigma_misfit,tmpMju,tmpNju,E,1,1,1)));
          En:=En+EnergyInPoint(SM,Sigma_misfit,tmpMju,tmpNju,E,dx*1e-10,dy*1e-10,dz*1e-10);
          //En:=En+En_tmp*1e-10*1e-10*1e-10;
          Inc(Counter);
          if Counter mod IterCount = 0 then begin
            Gauge1.Progress:=Gauge1.Progress+1;
            Application.ProcessMessages;
          end;
        end;
      end;
    end;
  end;

  CloseFile(NucleiFile);

  //����� ������� � ������ ����
  //ShowMessage(FloatToStr(En/((L)*(L2)*sqr(1e-10))));
  //
  //������ �� ������� ����
  //z:=10;
  if CompareValue(Worksheet.FilmThickness,H1)=GreaterThanValue then begin
    if z<=H1 then begin
      dz:=H2/Nz2;
    end
    else begin
      dz:=(Worksheet.FilmThickness-z)/Nz2;
    end;
    for k:=1 to Nz2 do begin
      z:=z+dz;
      y:=-dy/2;
      for j:=1 to N do begin
        x:=-dx/2;
        y:=y+dy;
        for i:=1 to N do begin
          x:=x+dx;
          SM:=GetStressMatrix(x,y,z);
          //if z=Worksheet.FilmThickness then ShowMessage(FloatToStr(SM[1][1]+Sigma_misfit));
          En:=En+EnergyInPoint(SM,Sigma_misfit,tmpMju,tmpNju,E,dx*1e-10,dy*1e-10,dz*1e-10);
          Inc(Counter);
          if Counter mod IterCount = 0 then begin
            Gauge1.Progress:=Gauge1.Progress+1;
            Application.ProcessMessages;
          end;
        end;
      end;
    end;
  end;
  //

  EditSummary.Text:=FloatToStr((En+En_nuc)/((L-dy)*(L2-dx)*sqr(1e-10)));
  En:=En/((L-dy)*(L2-dx)*sqr(1e-10));
  EditEnergy.Text:=FloatToStr(En);
  Gauge1.Progress:=0;
  //������ ����������� �������� � ����
  AssignFile(EnergyFile,'energy_results.txt');
  try
    Append(EnergyFile);
    Writeln(EnergyFile,IntToStr(Worksheet.DislocationFamiliesCount)+'; X � Y: '+IntToStr(N)+'; H1: '+FloatToStr(H1)+'; Nz1: '+IntToStr(Nz1)+'; H2: '+FloatToStr(H2)+'; �� Z: '+IntToStr(Nz2)+'; ������� ������: '+EditFilmThickness.Text+'; �������: '+EditEnergy.Text+' + '+EditNucleus.Text+' = '+EditSummary.Text);
  finally
    CloseFile(EnergyFile);
  end;
  //
  StatusBar1.Panels.Items[0].Text:='Status: Idle';
end;

procedure TForm1.N3Click(Sender: TObject);
var
  D_lim: extended;
  b,f: extended;
begin
  ReadFamilies;
  b:=abs(ScalarMult(Families[1].Burgers,NormVec(VecxVec(GetVector(Worksheet.p,Worksheet.q,Worksheet.r),Families[1].Direction)))); //����������� ������� ����������
  f:=Worksheet.aFilm/Worksheet.aSub-1;
  D_lim:=3*b/f/2;
  StringGrid1.Cells[3,StringGrid1.Selection.Top]:=FloatToStr(Round(D_lim*10)/10);
end;

procedure TForm1.N4Click(Sender: TObject);
var
  i: byte;
  b,f: extended;
  D_lim: extended;
begin
  ReadFamilies;
  //f:=Worksheet.aFilm/Worksheet.aSub-1;
  //f:=2*(Worksheet.aFilm-Worksheet.aSub)/(Worksheet.aSub+Worksheet.aFilm);
  f:=(Worksheet.aFilm-Worksheet.aSub)/Worksheet.aFilm; //��� ���� f ������� ������� � �� ������ � ����������� �������
  if Worksheet.DislocationFamiliesCount=2 then begin
    for i:=1 to Worksheet.DislocationFamiliesCount do begin
      D_lim:=abs(Families[i].BurgersProjection.y)/f;
      StringGrid1.Cells[3,i]:=FloatToStr(Round(D_lim*10)/10);
      //StringGrid1.Cells[3,i]:=FloatToStr(D_lim);
    end;
  end
  else begin
    for i:=1 to Worksheet.DislocationFamiliesCount do begin
      D_lim:=(3/2)*abs(Families[i].BurgersProjection.y)/f;
      StringGrid1.Cells[3,i]:=FloatToStr(Round(D_lim*10)/10);
      //StringGrid1.Cells[3,i]:=FloatToStr(D_lim);
    end;
  end;
end;

procedure TForm1.N5Click(Sender: TObject);
begin
  //Showmessage(IntToStr(StringGrid1.Selection.Top));
  ButtonAddFamily.Click;
  StringGrid1.Rows[Worksheet.DislocationFamiliesCount]:=StringGrid1.Rows[StringGrid1.Selection.Top];
  StringGrid1.Cells[0,Worksheet.DislocationFamiliesCount]:='          '+IntToStr(Worksheet.DislocationFamiliesCount);
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  if Edit2.Text<>'' then begin
    if CompareValue(StrToFloat(Edit2.Text),StrToFloat(EditFilmThickness.Text))=GreaterThanValue then
      Edit2.Text:=EditFilmThickness.Text;
  end;
end;

procedure TForm1.CheckBoxSolSolClick(Sender: TObject);
begin
  if CheckBoxSolSol.Checked then begin
    ComboBoxFilm2.Enabled:=true;
    LabelX.Enabled:=true;
    EditX.Enabled:=true;
  end
  else begin
    ComboBoxFilm2.Enabled:=false;
    LabelX.Enabled:=false;
    EditX.Enabled:=false;
  end;
  SetFilmParameter;
end;

procedure TForm1.EditXExit(Sender: TObject);
begin
  SetFilmParameter;
end;

procedure TForm1.ComboBoxFilm2Change(Sender: TObject);
begin
  SetFilmParameter;
end;

procedure TForm1.StringGrid1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbRight then
    mouse_event(MOUSEEVENTF_LEFTDOWN,0,0,0,0);
end;

function TForm1.GetContourName: string;
begin
  Result:='Err';
  if RadioButtonSquare.Checked then Result:='square'
  else if RadioButtonTriangle.Checked then Result:='triangle';
end;

procedure TForm1.SetContourChecked(ContourName: string);
begin
  if ContourName='square' then RadioButtonSquare.Checked:=true
  else if ContourName='triangle' then RadioButtonTriangle.Checked:=true;
end;

function TForm1.IsImageDislocationsUsed: boolean;
var
  i: byte;
begin
  Result:=false;
  for i:=1 to Worksheet.DislocationFamiliesCount do begin
    if Families[i].Height<>0 then begin
      Result:=true;
      exit;
    end;
  end;
end;

{ TNewThread }

procedure TNewThread.Calc;
begin
  if Form1.EditSummary.Text='alex' then Form1.Caption:='ok';
end;

procedure TNewThread.Execute;
var
  i: integer;
begin
  for i:=1 to 10000000 do begin
    Calc;
  end;
  Form1.Caption:='Finished';
end;

function TForm1.DeformationZZ(Sigma_XX, Sigma_YY, Sigma_ZZ, Sigma_misfit, tmpMju, tmpNju: extended): extended;
begin
  Result:=(1/(2*tmpMju*(1+tmpNju)))*(0-tmpNju*(Sigma_XX+Sigma_YY+2*Sigma_misfit));
end;

procedure TForm1.Button8Click(Sender: TObject);
var
  DataFile: TextFile;
  x0,x1,y0,y1,x,y,z,hx,hy: extended;
  z1,z2,dz,Delta_H: extended;
  Sigma_XX, Sigma_YY, Sigma_ZZ: extended;
  E: extended;
  k, m: integer;
  n: integer; //���������� ����� ����� ���
  i,j: integer;
  S1, S2: Matrix; //������ ����������
  Sigma_misfit: extended; //���������� ��������������
  f,b: extended; //�������� ��������������, ������ ��������
  D_max: extended;
  Counter,IterCount: integer; //�������, ��� ��������
  Mult: extended;
  SigmaSurf: Matrix;
begin
  SaveDialog1.Filter:='Data Files (*.dat)|*.dat';
  if SaveDialog1.Execute then begin
    StatusBar1.Panels.Items[0].Text:='Status: Processing..';
    Counter:=0; //��������� ������� � ������� ���������
    //f:=Worksheet.aFilm/Worksheet.aSub-1;
    f:=(Worksheet.aFilm-Worksheet.aSub)/Worksheet.aFilm;
    ReadFamilies;
    b:=ScalarMult(Families[1].Burgers,NormVec(VecxVec(GetVector(Worksheet.p,Worksheet.q,Worksheet.r),Families[1].Direction)) ); //����������� ������� ����������
    //D_max:=b/f;
    //D_max:=143.5;
    {for i:=1 to Worksheet.DislocationFamiliesCount do begin
      StringGrid1.Cells[3,i]:=FloatToStr(D_max);
    end;   }
    //Sigma_misfit:=tmpMju*f*(1+tmpNju)/(1-tmpNju);
    if IsImageDislocationsUsed then Mult:=2 else Mult:=1;
    Sigma_misfit:=SigmaMisfit(Worksheet.aSub,Worksheet.aFilm,tmpMju,tmpNju,Mult);
    //ShowMessage(FloatToStr(D_max)+' '+FloatToStr(Sigma_misfit));
    ReadFamilies;
    //z:=mx_StrToExt(EditHeight.Text);
    n:=StrToInt(EditPntCount.Text);
    IterCount:=Round(n*n/100);
    x0:=mx_StrToExt(EditX0.Text);
    x1:=mx_StrToExt(EditX1.Text);
    y0:=mx_StrToExt(EditY0.Text);
    y1:=mx_StrToExt(EditY1.Text);
    z1:=4;
    z2:=mx_StrToExt(EditHeight.Text);
    m:=101;
    hx:=(x1-x0)/(n-1);
    hy:=(y1-y0)/(n-1);
    Rewrite(DataFile,SaveDialog1.FileName);
    x:=x0; y:=y0; z:=z1;
    dz:=(z2-z1)/(m-1);
    for j:=1 to n do begin
      for i:=1 to n do begin
        z:=z1;
        Delta_H:=0;
        SigmaSurf:=GetStressMatrix(x,y,z2);
        for k:=1 to m do begin
          if z=z2 then break;
          S1:=GetStressMatrix(x,y,z);
          S2:=GetStressMatrix(x,y,z+dz);
          Sigma_XX:=(S1[1,1]+S2[1,1])/2;
          Sigma_YY:=(S1[2,2]+S2[2,2])/2;
          Sigma_ZZ:=(S1[3,3]+S2[3,3])/2;
          E:=DeformationZZ(Sigma_XX, Sigma_YY, Sigma_ZZ, 0, tmpMju, tmpNju);
          Delta_H:=Delta_H+(1+E)*dz;
     //   S:=GetStressMatrix(x,y,z);
          z:=z+dz;
        end;
        Writeln(DataFile,x,'; ',y,'; ',Delta_H,'; '{,S1[CI_X,CI_Y]}{+Sigma_misfit});
        Inc(Counter);
        if Counter mod IterCount = 0 then begin
          Gauge1.Progress:=Gauge1.Progress+1;
          Application.ProcessMessages;
        end;
        if hx=0 then break;
        x:=x+hx;
      end;
      if hy=0 then begin if j=1 then break; end;
      x:=x0; y:=y+hy;
    end;
    Gauge1.Progress:=0;
    CloseFile(DataFile);
    StatusBar1.Panels.Items[0].Text:='Status: Done';
  end;
end;


procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
begin
  if CompareValue(StrToFloat(EditFilmThickness.Text),StrToFloat(Edit2.Text))=LessThanValue then begin
    Edit2.Text:=EditFilmThickness.Text;
end;

end;

procedure TForm1.N6Click(Sender: TObject);
begin
  //ReadFamilies;
  StringGrid1.Cells[4,StringGrid1.Selection.Top]:=FloatToStr(mx_StrToExt(StringGrid1.Cells[3,StringGrid1.Selection.Top])*mx_StrToExt(EditOffset.Text)/100);
end;

end.

