unit dislmath;

interface

uses math, mxvectors;

function Sigma0(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended): extended; //������� ��� ����������� ���������� Sigma0 (����. 19.76, ����)
function SigmaZY_tilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
function SigmaYY_tilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
function SigmaZZ_tilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
function SigmaZY_edge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
function SigmaYY_edge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
function SigmaZZ_edge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
function SigmaZX_screw(BProjection: extended; Distance: extended; y,z: extended; Mju: extended): extended;
function SigmaYX_screw(BProjection: extended; Distance: extended; y,z: extended; Mju: extended): extended;
//function GetStressMatrixTilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended): Matrix; //����������� ������� ��� ��������� ���������
//function GetStressMatrixEdge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended): Matrix; //����������� ������� ��� ������� ���������
//function GetStressMatrixScrew(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended): Matrix; //����������� ������� ��� ������� ���������
function GetStressMatrixFamily(BProjectionX,BProjectionY,BProjectionZ: extended; Distance: extended; y,z: extended; Mju, Nju: extended): Matrix;
function EnergyInPoint(StressTenzor: Matrix; Sigma_misfit: extended; Mju,Nju,E: extended; dx,dy,dz: extended): extended; //��������� ������� �� ��������� ������� ����������
function EnergyNucleus(EdgeBurgers,ScrewBurgers,DislocationLength,Mju,Nju: extended): extended;
function SigmaMisfit(aSub,aFilm,Mju,Nju: extended; Multiplier: extended): extended; overload;
function SigmaMisfit(aSub,aFilm,Mju,Nju: extended; D: extended; Multiplier: extended): extended; overload;

function R_SigmaZZ_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
function R_SigmaYY_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
function R_SigmaXX_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
function R_SigmaZY_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
function R_SigmaZX_screw(BProjection: extended; Distance: extended; y,z, H: extended; Mju: extended): extended;
function R_SigmaYX_screw(BProjection: extended; Distance: extended; y,z, H: extended; Mju: extended): extended;
function GetStressMatrixFamilyR(BProjectionX,BProjectionY,BProjectionZ: extended; Distance: extended; y,z: extended; H: extended; Mju, Nju: extended): Matrix;
function DisplacementZ(BurgersEdge, FilmHeight, Distance, y: extended): extended;


implementation

function Sigma0(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended): extended;
//===============================================================================================================
//  ������� ��� ����������� ���������� Sigma0 (����. 19.76, ����)
//  ��� ���������� ������� ����� ��� z � ������� �������� �.�������� ������ �� ��� x
//  ���������:
//        BProjection: extended - �������� �������� ������� �������� �� ��� x
//        Distance: extended - ���������� ����� ������������
//        y,z - ���������� ����� � ���������, z - ��������������� ��-�� ����������, y - ��������������� ����� ����������
//  ������������ ���������:
//      ������� ���������� ���������� sigma0
//===============================================================================================================
begin
  if (Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance))<>0 then begin
    Result:=Mju*BProjection/( 2*Distance*(1-Nju)*sqr(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance)));
  end
  else Result:=0;
end;

function SigmaZY_tilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
begin
  Result:=Sigma0*2*Pi*z/Distance*(Cosh(2*Pi*z/Distance)*cos(2*Pi*y/Distance)-1);
end;

function SigmaZZ_tilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
begin
  Result:=-Sigma0*sin(2*Pi*y/Distance)*(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance)+2*Pi*z/Distance*Sinh(2*Pi*z/Distance));
end;

function SigmaYY_tilted(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
begin
  Result:=-Sigma0*sin(2*Pi*y/Distance)*(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance)-2*Pi*z/Distance*Sinh(2*Pi*z/Distance));
end;

function SigmaZY_edge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
begin
  Result:=Sigma0*sin(2*Pi*y/Distance)*(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance)-2*Pi*z/Distance*Sinh(2*Pi*z/Distance));
end;

function SigmaZZ_edge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
begin
  Result:=-Sigma0*2*Pi*z/Distance*(Cosh(2*Pi*z/Distance)*cos(2*Pi*y/Distance)-1);
end;

function SigmaYY_edge(BProjection: extended; Distance: extended; y,z: extended; Mju, Nju: extended; Sigma0: extended): extended;
begin
  Result:=Sigma0*(2*Sinh(2*Pi*z/Distance)*(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance))-2*Pi*z/Distance*(Cosh(2*Pi*z/Distance)*cos(2*Pi*y/Distance)-1) );
end;

function SigmaZX_screw(BProjection: extended; Distance: extended; y,z: extended; Mju: extended): extended;
begin
  if BProjection<>0 then begin
    Result:=-Mju*BProjection/(2*Distance)*(sin(2*Pi*y/Distance))/(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance));
  end
  else begin
    Result:=0;
  end;
end;

function SigmaYX_screw(BProjection: extended; Distance: extended; y,z: extended; Mju: extended): extended;
begin
  if BProjection<>0 then begin
    Result:=Mju*BProjection/(2*Distance)*(Sinh(2*Pi*z/Distance))/(Cosh(2*Pi*z/Distance)-cos(2*Pi*y/Distance));
  end
  else begin
    Result:=0;
  end;
end;

function GetStressMatrixFamily(BProjectionX,BProjectionY,BProjectionZ: extended; Distance: extended; y,z: extended; Mju, Nju: extended): Matrix;
//===============================================================================================================
//  ������� ��� ������� ������� ���������� � ����� y,z ��� ������� ������������ ����������� ����������
//  ���������:
//        BProjectionX,BProjectionY,BProjectionZ: extended - �������� ������� �������� �� �������������� ��� ��� ���������,
//                                                           ������� � �������� ��������� ��������������
//        Distance: extended - ���������� ����� ������������
//        y,z - ���������� ����� � ���������, z - ��������������� ��-�� ����������, y - ��������������� ����� ����������
//  ������������ ���������:
//      ������� ���������� ������� ����������
//===============================================================================================================
var
  S_ZY_nakl, S_ZY_edge, S_ZX_vint, S_YX_vint, S_ZZ_nakl, S_ZZ_edge, S_YY_nakl, S_YY_edge: extended;
  S0_nakl, S0_edge: extended;
begin
  S0_nakl:=Sigma0(BProjectionZ,Distance,y,z,Mju,Nju);
  S0_edge:=Sigma0(BProjectionY,Distance,y,z,Mju,Nju);
  //���������� ��� ��������� ����������
  S_ZY_nakl:=SigmaZY_tilted(BProjectionZ,Distance,y,z,Mju,Nju,S0_nakl);
  S_ZZ_nakl:=SigmaZZ_tilted(BProjectionZ,Distance,y,z,Mju,Nju,S0_nakl);
  S_YY_nakl:=SigmaYY_tilted(BProjectionZ,Distance,y,z,Mju,Nju,S0_nakl);
  //���������� ��� ������� ����������
  S_ZY_edge:=SigmaZY_edge(BProjectionY,Distance,y,z,Mju,Nju,S0_edge);
  S_ZZ_edge:=SigmaZZ_edge(BProjectionY,Distance,y,z,Mju,Nju,S0_edge);
  S_YY_edge:=SigmaYY_edge(BProjectionY,Distance,y,z,Mju,Nju,S0_edge);
  //���������� ��� �������� ����������
  S_ZX_vint:=SigmaZX_screw(BProjectionX,Distance,y,z,Mju);
  S_YX_vint:=SigmaYX_screw(BProjectionX,Distance,y,z,Mju);
  
  Result[1][1]:=Nju*(S_YY_edge+S_YY_nakl+S_ZZ_edge+S_ZZ_nakl);
  Result[1][2]:=S_YX_vint;
  Result[1][3]:=S_ZX_vint;
  Result[2][1]:=Result[1][2];
  Result[2][2]:=S_YY_nakl+S_YY_edge;
  Result[2][3]:=S_ZY_nakl+S_ZY_edge;
  Result[3][1]:=Result[1][3];
  Result[3][2]:=Result[2][3];
  Result[3][3]:=S_ZZ_nakl+S_ZZ_edge;
end;

function EnergyInPoint(StressTenzor: Matrix; Sigma_misfit: extended; Mju,Nju,E: extended; dx,dy,dz: extended): extended;
//===============================================================================================================
//  ������� ��� ������� ������� �� ��������� ������� ����������
//  ���������:
//        StressTenzor: Matrix - ������ ����������
//  ������������ ���������:
//      ������� ������� � �����
//===============================================================================================================
begin
  Result:=(0.5/E)*(sqr(StressTenzor[1,1]+Sigma_misfit)+sqr(StressTenzor[2,2]+Sigma_misfit)+sqr(StressTenzor[3,3]))
          -Nju/E*((StressTenzor[1,1]+Sigma_misfit)*(StressTenzor[2,2]+Sigma_misfit)+(StressTenzor[2,2]+Sigma_misfit)*StressTenzor[3,3]+StressTenzor[3,3]*(StressTenzor[1,1]+Sigma_misfit))
          +(0.5/Mju)*(sqr(StressTenzor[1,2])+sqr(StressTenzor[1,3])+sqr(StressTenzor[2,3]));
  Result:=Result*dx*dy*dz;
end;

function EnergyNucleus(EdgeBurgers,ScrewBurgers,DislocationLength,Mju,Nju: extended): extended;
begin
  Result:=Mju/4/Pi*(sqr(EdgeBurgers*1e-10)/(1-Nju)+sqr(ScrewBurgers*1e-10))*DislocationLength*1e-10;
end;

function SigmaMisfit(aSub,aFilm,Mju,Nju: extended; D: extended; Multiplier: extended): extended; overload;
var
  Ro,D0,a: extended;
begin
  D0:=D;
  Ro:=D0/D;
  a:=Ro*(aFilm-aSub)+aSub;
  Result:=Multiplier*Mju*((aSub-a)/a)*(1+Nju)/(1-Nju);
end;

function SigmaMisfit(aSub,aFilm,Mju,Nju: extended; Multiplier: extended): extended; overload;
begin
  Result:=Multiplier*Mju*((aSub-aFilm)/aFilm)*(1+Nju)/(1-Nju);
end;

//������� ��� ���������� ���������� �� "��������"

function R_SigmaZZ_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended; //����������� Andrews, LeSar
  L: extended; //���������� ����� ������������
begin
  D:=Mju/(2*Pi*(1-Nju));
  L:=Distance;
  Result:=-D*BProjection*Pi/Distance*( -2*Pi*z/L*( cosh(2*Pi*z/L)*cos(2*Pi*y/L)-1 )/( sqr(cosh(2*Pi*(z)/L)-cos(2*Pi*y/L)) )
                                      -2*Pi*(z-2*H)/L*( cosh(2*Pi*(z-2*H)/L)*cos(2*Pi*y/L)-1 )/( sqr(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L)) )
                                      +2*Pi*(z-H)/L*( 3*cos(2*Pi*y/L)-cosh(2*Pi*(z-2*H)/L)*(3+cos(4*Pi*y/L)) )/( power(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L),3) )
                                      +2*Pi*(z-H)/L*( ( cosh(4*Pi*(z-2*H)/L)+2*Pi*H/L*sinh(4*Pi*(z-2*H)/L) )*cos(2*Pi*y/L)-2*Pi*H/L*sinh(2*Pi*(z-2*H)/L)*(3-cos(4*Pi*y/L)) )/( power(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L),3) )
                                     );
end;

function R_SigmaYY_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended; //����������� Andrews, LeSar
  L: extended; //���������� ����� ������������
begin
  D:=Mju/(2*Pi*(1-Nju));
  L:=Distance;
  Result:=-D*BProjection*Pi/Distance*( ( 2*Pi*z/L*( cosh(2*Pi*z/L)*cos(2*Pi*y/L)-1 )+2*sinh(2*Pi*z/L)*cos(2*Pi*y/L)-sinh(4*Pi*z/L) )/( sqr(cosh(2*Pi*(z)/L)-cos(2*Pi*y/L)) )
                                      +( 2*Pi*(z+2*H)/L*(cosh(2*Pi*(z-2*H)/L)*cos(2*Pi*y/L)-1)-2*sinh(2*Pi*(z-2*H)/L)*cos(2*Pi*y/L)+sinh(4*Pi*(z-2*H)/L) )/( sqr(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L)) )
                                      -2*Pi*(z-H)/L*( 3*cos(2*Pi*y/L)-cosh(2*Pi*(z-2*H)/L)*(3+cos(4*Pi*y/L)) )/( power(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L),3) )
                                      -2*Pi*(z-H)/L*( (cosh(4*Pi*(z-2*H)/L)+2*Pi*H/L*sinh(4*Pi*(z-2*H)/L))*cos(2*Pi*y/L)-2*Pi*H/L*sinh(2*Pi*(z-2*H)/L)*(3-cos(4*Pi*y/L)) )/( power(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L),3) )
                                    );
end;

function R_SigmaXX_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended; //����������� Andrews, LeSar
  L: extended; //���������� ����� ������������
begin
  D:=Mju/(2*Pi*(1-Nju));
  L:=Distance;
  Result:=-D*BProjection*Pi*Nju/Distance*( -2*sinh(2*Pi*z/L)/( cosh(2*Pi*z/L)-cos(2*Pi*y/L) )
                                          +( -8*Pi*H/L+8*Pi*H/L*cosh(2*Pi*(z-2*H)/L)*cos(2*Pi*y/L)-2*sinh(2*Pi*(z-2*H)/L)*cos(2*Pi*y/L)+sinh(4*Pi*(z-2*H)/L) )/sqr(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L))
                                    );
end;

function R_SigmaZY_edge(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended; //����������� Andrews, LeSar
  L: extended; //���������� ����� ������������
begin
  D:=Mju/(2*Pi*(1-Nju));
  L:=Distance;
  Result:=-D*BProjection*Pi/Distance*( -sin(2*Pi*y/L)*( 2*Pi*z/L*sinh(2*Pi*z/L)+cos(2*Pi*y/L)-cosh(2*Pi*z/L) )/( sqr(cosh(2*Pi*z/L)-cos(2*Pi*y/L)) )
                                      +sin(2*Pi*y/L)*( -2*Pi*z/L*sinh(2*Pi*(z-2*H)/L)+cos(2*Pi*y/L)-cosh(2*Pi*(z-2*H)/L) )/( sqr(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L)) )
                                      +4*Pi*(z-H)/L*sin(2*Pi*y/L)*( 2*Pi*H/L*cosh(2*Pi*(z-2*H)/L)*cos(2*Pi*y/L)-3*Pi*H/L+Pi*H/L*cosh(4*Pi*(z-2*H)/L)+sinh(2*Pi*(z-2*H)/L)*( cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L) ) )/( power(cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L),3) )
                                    );
end;

function R_SigmaZX_screw(BProjection: extended; Distance: extended; y,z, H: extended; Mju: extended): extended;
var
  L: extended; //���������� ����� ������������
begin
  L:=Distance;
  Result:=-Mju*BProjection/Distance/2*( -sin(2*Pi*y/L)/( cosh(2*Pi*z/L)-cos(2*Pi*y/L) )
                                       +sin(2*Pi*y/L)/( cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L) )
                                    );
end;

function R_SigmaYX_screw(BProjection: extended; Distance: extended; y,z, H: extended; Mju: extended): extended;
var
  L: extended; //���������� ����� ������������
begin
  L:=Distance;
  Result:=-Mju*BProjection/Distance/2*( sinh(2*Pi*z/L)/( cosh(2*Pi*z/L)-cos(2*Pi*y/L) )
                                       -sinh(2*Pi*(z-2*H)/L)/( cosh(2*Pi*(z-2*H)/L)-cos(2*Pi*y/L) )
                                    );
end;





function R_SigmaZZ_tilted(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended; //����������� Andrews, LeSar
begin
  D:=Mju/(2*Pi*(1-Nju));
  Result:=-D*BProjection*Pi/Distance*(sin(2*Pi*y/Distance)*(cos(2*Pi*y/Distance)-cosh(2*Pi*(z)/Distance)-2*Pi*(z)/Distance*sinh(2*Pi*(z)/Distance))/(sqr(cosh(2*Pi*(z)/Distance)-cos(2*Pi*y/Distance)))
                                    -sin(2*Pi*y/Distance)*(cos(2*Pi*y/Distance)-cosh(2*Pi*(z-2*H)/Distance)-2*Pi*(z-2*H)/Distance*sinh(2*Pi*(z-2*H)/Distance))/(sqr(cosh(2*Pi*(z-2*H)/Distance)-cos(2*Pi*y/Distance)))
                                    -4*Pi*Pi*z/Distance*H/Distance*sin(2*Pi*y/Distance)*(-3+cosh(4*Pi*(z-2*H)/Distance)+2*cosh(2*Pi*(z-2*H)/Distance)*cos(2*Pi*y/Distance))/(Power(cosh(2*Pi*(z-2*H)/Distance)-cos(2*Pi*y/Distance),3)));
end;

function R_SigmaYY_tilted(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended; //����������� Andrews, LeSar
begin
  //D:=Mju/(2*Pi*(1-Nju));
  Result:=0;
end;

function R_SigmaZY_tilted(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended;
begin
  //D:=Mju/(2*Pi*(1-Nju));
  Result:=0;
end;

function R_SigmaXX_tilted(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended;
begin
  //D:=Mju/(2*Pi*(1-Nju));
  Result:=0;
end;

function R_SigmaZX_tilted(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
var
  D: extended;
begin
  //D:=Mju/(2*Pi*(1-Nju));
  Result:=0;
end;

function R_SigmaYX_tilted(BProjection: extended; Distance: extended; y,z, H: extended; Mju, Nju: extended): extended;
begin
  Result:=R_SigmaZX_tilted(BProjection,Distance,y,z,H,Mju,Nju);
end;

function GetStressMatrixFamilyR(BProjectionX,BProjectionY,BProjectionZ: extended; Distance: extended; y,z: extended; H: extended; Mju, Nju: extended): Matrix;
//===============================================================================================================
//  ������� ��� ������� ������� ���������� �� �������� � ����� y,z ��� ������� ������������ ����������� ����������
//  ���������:
//        BProjectionX,BProjectionY,BProjectionZ: extended - �������� ������� �������� �� �������������� ��� ��� ���������,
//                                                           ������� � �������� ��������� ��������������
//        Distance: extended - ���������� ����� ������������
//        y,z - ���������� ����� � ���������, z - ��������������� ��-�� ����������, y - ��������������� ����� ����������
//  ������������ ���������:
//      ������� ���������� ������� ����������
//===============================================================================================================
var
  S_ZY_nakl, S_ZY_edge, S_ZX_vint, S_YX_vint, S_ZZ_nakl, S_ZZ_edge, S_YY_nakl, S_YY_edge, S_XX_edge: extended;
begin
  //���������� ��� ��������� ����������
  S_ZY_nakl:=0;
  S_ZZ_nakl:=0;
  S_YY_nakl:=0;
  //���������� ��� ������� ����������
  S_ZY_edge:=R_SigmaZY_edge(BProjectionY,Distance,y,z,H,Mju,Nju);
  S_ZZ_edge:=R_SigmaZZ_edge(BProjectionY,Distance,y,z,H,Mju,Nju);
  S_YY_edge:=R_SigmaYY_edge(BProjectionY,Distance,y,z,H,Mju,Nju);
  S_XX_edge:=R_SigmaXX_edge(BProjectionY,Distance,y,z,H,Mju,Nju);
  //���������� ��� �������� ����������
  S_ZX_vint:=R_SigmaZX_screw(BProjectionX,Distance,y,z,H,Mju);
  S_YX_vint:=R_SigmaYX_screw(BProjectionX,Distance,y,z,H,Mju);
  
  Result[1][1]:=S_XX_edge;
  Result[1][2]:=S_YX_vint;
  Result[1][3]:=S_ZX_vint;
  Result[2][1]:=Result[1][2];
  Result[2][2]:=S_YY_nakl+S_YY_edge;
  Result[2][3]:=S_ZY_nakl+S_ZY_edge;
  Result[3][1]:=Result[1][3];
  Result[3][2]:=Result[2][3];
  Result[3][3]:=S_ZZ_nakl+S_ZZ_edge;
end;

function DisplacementZ(BurgersEdge, FilmHeight, Distance, y: extended): extended;
//===============================================================================================================
//  ������� ��� ������� �������� ��������������� �� �� �������� � ��� ������� ������������ ����������� ����������
//  ���������:
//        BurgersEdge: extended - ������� ���������� ������� �������� ������� � ��
//        FilmHeight: extended - ������� ������
//        Distance: extended - ���������� ����� ������������
//        y - ���������� ����� � ��������� ��������������� ����� ���������� � ��������� ��
//  ������������ ���������:
//      ������� ���������� �������� ��������
//===============================================================================================================
begin
  Result:=BurgersEdge*(FilmHeight)/Distance*sinh(2*Pi*FilmHeight/Distance)/(cosh(2*Pi*FilmHeight/Distance)-cos(2*Pi*y/Distance));
end;


end.
